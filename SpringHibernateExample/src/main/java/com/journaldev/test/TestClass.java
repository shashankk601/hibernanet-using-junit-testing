package com.journaldev.test;

import  java.util.List; 

import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.journaldev.dao.PersonDAO;
import com.journaldev.model.Person;

public class TestClass {
	
	 @Test
    public void testMethod() {
		 System.out.println("Inside TestMethod!!!");
		 ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		 
		 PersonDAO personDAO = context.getBean(PersonDAO.class);
		 List<Person> list = personDAO.list();
		 
		 Person personForTesting = new Person();
		 
		 for (Person p : list) {
			 if(p.getName().matches("Shashank")) {
				 personForTesting = p;
				 System.out.println("Inside for loop !!!"+p.getId()+" and "+p.getName() );
				 
			 }
		 }
		 Assert.assertEquals(10,personForTesting.getId());
		 context.close();
		
	 }
}
