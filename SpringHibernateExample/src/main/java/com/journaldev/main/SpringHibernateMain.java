package com.journaldev.main;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.journaldev.dao.PersonDAO;
import com.journaldev.model.Person;

import org.junit.*;


public class SpringHibernateMain {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		
		PersonDAO personDAO = context.getBean(PersonDAO.class);
		
		Person person = new Person();
		person.setName("Shashank"); person.setCountry("India");
		
		personDAO.save(person);
		
		System.out.println("Person::"+person);
		
		List<Person> list = personDAO.list();
		
		for(Person p : list){
			System.out.println("Person List::"+p);
		}
		
		Person personRequired = new Person();
		
		List<Person> personForTesting  = personDAO.list();
		for(Person p : personForTesting){
			if (p.getName().matches("Shashank")) {
				 personRequired = p;
			}
		}
		testMethod(personRequired);
		
		context.close();
		
	}
	
	@Test
	public static void testMethod(Person person) {
		System.out.println("Inside Test Method !!");
		Assert.assertEquals(1, person.getId());
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	}

}
